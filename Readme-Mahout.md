To adapt this project into an existing Apache Mahout development environment, 
you have to (1) create links of the source folders under mahout source tree, and
(2) update the config files of Mahout. Assuming mahout development directory is 
`~/workspace/mahout` and bigfim is in `~/workspace/bigfim` , then the steps are 
as follows:

1. First create the links for the source folders:
```
    cd ~/workspace/mahout/core/src/main/java/org/apache/mahout/
    ln -s ~/workspace/bigfim/src/org/apache/mahout/fpm .
    cd ~/workspace/mahout/core/src/test/java/org/apache/mahout/
    ln -s ~/workspace/bigfim/test/org/apache/mahout/fpm .
```
2. Introduce the new classes to mahout, add the contents of the file 
```
    ~/workspace/bigfim/resources/driver.classes.default.props
```
to the end of the file: 
```
~/workspace/mahout/src/conf/driver.classes.default.props
```
3. Copy the argument files to mahout dev folder:
```
    cp ~/workspace/bigfim/resources/bigfim.props ~/workspace/mahout/src/conf
    cp ~/workspace/bigfim/resources/disteclat.props ~/workspace/mahout/src/conf
    cp ~/workspace/bigfim/resources/triedump.props ~/workspace/mahout/src/conf
```
4. Copy the test resources to mahout dev folder:
```
    cp ~/workspace/bigfim/resources/retail_maximal_sup100_min2.dat ~/workspace/mahout/core/src/test/resources/
```
5. Rebuild the project using maven:
```
    cd ~/workspace/mahout/
    mvn clean install
```

That's it.
