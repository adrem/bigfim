package org.apache.mahout.fpm.util;

public class FIMOptions {
  
  public static final String INPUT_FILE_KEY = "input_file";
  public static final String MIN_SUP_KEY = "minsup";
  public static final String NUMBER_OF_LINES_KEY = "number_of_lines_read";
  public static final String NUMBER_OF_MAPPERS_KEY = "number_of_mappers";
  public static final String OUTPUT_DIR_KEY = "output_dir";
  public static final String PREFIX_LENGTH_KEY = "prefix_length";
  public static final String SUBDB_SIZE = "sub_db_size";
  public static final String DELIMITER_KEY = "delimiter";
  
  public String inputFile;
  public String outputDir;
  public int minSup;
  public int prefixLength;
  public int nrMappers;
  public String delimiter;
  
  @Override
  public String toString() {
    return "FIMOptions [inputFile=" + inputFile + ", outputPath=" + outputDir + ", minSup=" + minSup
        + ", prefixLength=" + prefixLength + ", nrMappers=" + nrMappers + ", delimiter=" + delimiter + "]";
  }
}