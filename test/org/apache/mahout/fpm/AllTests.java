/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.mahout.fpm;

import org.apache.mahout.fpm.bigfim.AprioriPhaseMapperTest;
import org.apache.mahout.fpm.bigfim.AprioriPhaseReducerTest;
import org.apache.mahout.fpm.bigfim.ComputeTidListMapperTest;
import org.apache.mahout.fpm.bigfim.ComputeTidListReducerTest;
import org.apache.mahout.fpm.eclat.EclatMinerTest;
import org.apache.mahout.fpm.eclat.util.ItemTest;
import org.apache.mahout.fpm.eclat.util.SplitByKTextInputFormatTest;
import org.apache.mahout.fpm.eclat.util.TrieDumperTest;
import org.apache.mahout.fpm.util.DbTransposerTest;
import org.apache.mahout.fpm.util.ToolsTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({FPMDriversTest.class, AprioriPhaseMapperTest.class, AprioriPhaseReducerTest.class,
    ComputeTidListMapperTest.class, ComputeTidListReducerTest.class, EclatMinerTest.class, ItemTest.class,
    SplitByKTextInputFormatTest.class, TrieDumperTest.class, DbTransposerTest.class, ToolsTest.class})
public class AllTests {}
